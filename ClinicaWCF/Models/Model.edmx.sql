
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/28/2015 21:09:39
-- Generated from EDMX file: C:\Users\anthradk\documents\visual studio 2015\Projects\ClinicaWCF\ClinicaWCF\Models\Model.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [ClinicaDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_PersonaPaciente]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Paciente] DROP CONSTRAINT [FK_PersonaPaciente];
GO
IF OBJECT_ID(N'[dbo].[FK_PersonaEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Empleado] DROP CONSTRAINT [FK_PersonaEmpleado];
GO
IF OBJECT_ID(N'[dbo].[FK_TipoEmpleadoEmpleado]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Empleado] DROP CONSTRAINT [FK_TipoEmpleadoEmpleado];
GO
IF OBJECT_ID(N'[dbo].[FK_EmpleadoUsuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_EmpleadoUsuario];
GO
IF OBJECT_ID(N'[dbo].[FK_RolUsuario]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Usuario] DROP CONSTRAINT [FK_RolUsuario];
GO
IF OBJECT_ID(N'[dbo].[FK_PacienteCita]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cita] DROP CONSTRAINT [FK_PacienteCita];
GO
IF OBJECT_ID(N'[dbo].[FK_AgendaCita]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Cita] DROP CONSTRAINT [FK_AgendaCita];
GO
IF OBJECT_ID(N'[dbo].[FK_EmpleadoAgenda]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Agenda] DROP CONSTRAINT [FK_EmpleadoAgenda];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Persona]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Persona];
GO
IF OBJECT_ID(N'[dbo].[Paciente]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Paciente];
GO
IF OBJECT_ID(N'[dbo].[Empleado]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Empleado];
GO
IF OBJECT_ID(N'[dbo].[Cita]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Cita];
GO
IF OBJECT_ID(N'[dbo].[Agenda]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Agenda];
GO
IF OBJECT_ID(N'[dbo].[TipoEmpleado]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TipoEmpleado];
GO
IF OBJECT_ID(N'[dbo].[Usuario]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Usuario];
GO
IF OBJECT_ID(N'[dbo].[Rol]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Rol];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Persona'
CREATE TABLE [dbo].[Persona] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Apellido] nvarchar(max)  NOT NULL,
    [Identificacion] nvarchar(max)  NOT NULL,
    [FNacimiento] datetime  NOT NULL,
    [Telefono] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL,
    [Direccion] nvarchar(max)  NOT NULL,
    [Genero] nvarchar(max)  NOT NULL,
    [TipoDocumento] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Paciente'
CREATE TABLE [dbo].[Paciente] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NULL,
    [PersonaId] int  NOT NULL,
    [FRegistro] datetime  NOT NULL
);
GO

-- Creating table 'Empleado'
CREATE TABLE [dbo].[Empleado] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FRegistro] datetime  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [PersonaId] int  NOT NULL,
    [TipoEmpleadoId] int  NOT NULL
);
GO

-- Creating table 'Cita'
CREATE TABLE [dbo].[Cita] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FReserva] datetime  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [PacienteId] int  NOT NULL,
    [AgendaId] int  NOT NULL,
    [FAsignada] datetime  NULL
);
GO

-- Creating table 'Agenda'
CREATE TABLE [dbo].[Agenda] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FAgenda] datetime  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL,
    [EmpleadoId] int  NOT NULL
);
GO

-- Creating table 'TipoEmpleado'
CREATE TABLE [dbo].[TipoEmpleado] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL,
    [Estado] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Usuario'
CREATE TABLE [dbo].[Usuario] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Password] nvarchar(max)  NOT NULL,
    [EmpleadoId] int  NOT NULL,
    [RolId] int  NOT NULL
);
GO

-- Creating table 'Rol'
CREATE TABLE [dbo].[Rol] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Descripcion] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Persona'
ALTER TABLE [dbo].[Persona]
ADD CONSTRAINT [PK_Persona]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Paciente'
ALTER TABLE [dbo].[Paciente]
ADD CONSTRAINT [PK_Paciente]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Empleado'
ALTER TABLE [dbo].[Empleado]
ADD CONSTRAINT [PK_Empleado]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Cita'
ALTER TABLE [dbo].[Cita]
ADD CONSTRAINT [PK_Cita]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Agenda'
ALTER TABLE [dbo].[Agenda]
ADD CONSTRAINT [PK_Agenda]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TipoEmpleado'
ALTER TABLE [dbo].[TipoEmpleado]
ADD CONSTRAINT [PK_TipoEmpleado]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [PK_Usuario]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Rol'
ALTER TABLE [dbo].[Rol]
ADD CONSTRAINT [PK_Rol]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [PersonaId] in table 'Paciente'
ALTER TABLE [dbo].[Paciente]
ADD CONSTRAINT [FK_PersonaPaciente]
    FOREIGN KEY ([PersonaId])
    REFERENCES [dbo].[Persona]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonaPaciente'
CREATE INDEX [IX_FK_PersonaPaciente]
ON [dbo].[Paciente]
    ([PersonaId]);
GO

-- Creating foreign key on [PersonaId] in table 'Empleado'
ALTER TABLE [dbo].[Empleado]
ADD CONSTRAINT [FK_PersonaEmpleado]
    FOREIGN KEY ([PersonaId])
    REFERENCES [dbo].[Persona]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PersonaEmpleado'
CREATE INDEX [IX_FK_PersonaEmpleado]
ON [dbo].[Empleado]
    ([PersonaId]);
GO

-- Creating foreign key on [TipoEmpleadoId] in table 'Empleado'
ALTER TABLE [dbo].[Empleado]
ADD CONSTRAINT [FK_TipoEmpleadoEmpleado]
    FOREIGN KEY ([TipoEmpleadoId])
    REFERENCES [dbo].[TipoEmpleado]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TipoEmpleadoEmpleado'
CREATE INDEX [IX_FK_TipoEmpleadoEmpleado]
ON [dbo].[Empleado]
    ([TipoEmpleadoId]);
GO

-- Creating foreign key on [EmpleadoId] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_EmpleadoUsuario]
    FOREIGN KEY ([EmpleadoId])
    REFERENCES [dbo].[Empleado]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoUsuario'
CREATE INDEX [IX_FK_EmpleadoUsuario]
ON [dbo].[Usuario]
    ([EmpleadoId]);
GO

-- Creating foreign key on [RolId] in table 'Usuario'
ALTER TABLE [dbo].[Usuario]
ADD CONSTRAINT [FK_RolUsuario]
    FOREIGN KEY ([RolId])
    REFERENCES [dbo].[Rol]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RolUsuario'
CREATE INDEX [IX_FK_RolUsuario]
ON [dbo].[Usuario]
    ([RolId]);
GO

-- Creating foreign key on [PacienteId] in table 'Cita'
ALTER TABLE [dbo].[Cita]
ADD CONSTRAINT [FK_PacienteCita]
    FOREIGN KEY ([PacienteId])
    REFERENCES [dbo].[Paciente]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PacienteCita'
CREATE INDEX [IX_FK_PacienteCita]
ON [dbo].[Cita]
    ([PacienteId]);
GO

-- Creating foreign key on [AgendaId] in table 'Cita'
ALTER TABLE [dbo].[Cita]
ADD CONSTRAINT [FK_AgendaCita]
    FOREIGN KEY ([AgendaId])
    REFERENCES [dbo].[Agenda]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AgendaCita'
CREATE INDEX [IX_FK_AgendaCita]
ON [dbo].[Cita]
    ([AgendaId]);
GO

-- Creating foreign key on [EmpleadoId] in table 'Agenda'
ALTER TABLE [dbo].[Agenda]
ADD CONSTRAINT [FK_EmpleadoAgenda]
    FOREIGN KEY ([EmpleadoId])
    REFERENCES [dbo].[Empleado]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmpleadoAgenda'
CREATE INDEX [IX_FK_EmpleadoAgenda]
ON [dbo].[Agenda]
    ([EmpleadoId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------