﻿using ClinicaWCF.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.ServiceModel.Web;

namespace ClinicaWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "ClienteServicio" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione ClienteServicio.svc o ClienteServicio.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ClienteServicio : IClienteServicio
    {
        public void ActualizarPersona(Persona persona)
        {
            try
            {
                using (var db = new ModelContainer())
                {
                    var model = db.Persona.SingleOrDefault(x => x.Id == persona.Id);

                    if (model == null)
                    {
                        throw new Exception();
                    }

                    //model.Nombre = persona.Nombre;
                    //model.Apellido = persona.Apellido;
                    //model.Identificacion = persona.Identificacion;
                    //model.FNacimiento = persona.FNacimiento;
                    model.Telefono = persona.Telefono;
                    model.Correo = persona.Correo;
                    model.Direccion = persona.Direccion;
                    model.Genero = persona.Genero;
                    //model.TipoDocumento = persona.TipoDocumento;

                    if (db.ChangeTracker.HasChanges())
                    {
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public Paciente AutenticarPaciente(Paciente paciente)
        {
            Paciente model = null;

            try
            {
                using (var db = new ModelContainer())
                {
                    model = db.Paciente.SingleOrDefault(x => x.UserName == paciente.UserName && x.Password == paciente.Password);
                }

                if (model == null)
                    throw new Exception();

                model.Password = string.Empty;
                return model;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<Agenda> ConsultarAgendaByEmpleadoOdontologo(string id)
        {
            List<Agenda> list = new List<Agenda>();

            try
            {
                int empleadoId = Convert.ToInt32(id);

                using (var db = new ModelContainer())
                {
                    list = db.Agenda
                        //.Include(x=>x.Empleado)
                        .Where(x => x.Empleado.PersonaId == empleadoId)
                        .OrderBy(x => x.FAgenda)
                        .ToList();
                }

                if (list.Count == 0)
                    throw new Exception();

                return list;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<Cita> ConsultarCitasByPaciente(string userName)
        {
            List<Cita> list = new List<Cita>();

            try
            {
                using (var db = new ModelContainer())
                {
                    list = db.Cita
                        //.Include(x => x.Paciente)
                        .Where(x => x.Paciente.UserName == userName)
                        .OrderBy(x => x.FReserva)
                        .ToList();
                }

                if (list.Count  == 0)
                    throw new Exception();

                return list;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public List<Persona> ConsultarPersonaEmpleadoOdontologo()
        {
            List<Persona> list = new List<Persona>();

            try
            {
                using (var db = new ModelContainer())
                {
                    list = db.Empleado
                        .Include(x => x.Persona)
                        .Where(x => x.TipoEmpleadoId == 1)
                        .Select(x => x.Persona)
                        .ToList();
                }

                if (list.Count == 0)
                    throw new Exception();

                return list;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public Persona ConsultarPersona(string id)
        {
            Persona model = null;

            try
            {
                int personaId = Convert.ToInt32(id);

                using (var db = new ModelContainer())
                {
                    model = db.Persona.SingleOrDefault(x => x.Id == personaId);
                }

                if (model == null)
                    throw new Exception();

                return model;
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public void EliminarCita(string id)
        {
            try
            {
                int citaId = Convert.ToInt32(id);

                using (var db = new ModelContainer())
                {
                    var model = db.Cita.SingleOrDefault(x => x.Id == citaId);

                    if (model == null)
                        throw new Exception();

                    db.Cita.Remove(model);
                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public void RegistrarCita(Cita cita)
        {
            try
            {
                using (var db = new ModelContainer())
                {
                    cita.FReserva = DateTime.Now;
                    cita.Estado = "A";
                    db.Cita.Add(cita);

                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }

        public void RegistrarPersona(Persona persona)
        {
            try
            {
                using (var db = new ModelContainer())
                {
                    db.Persona.Add(persona);
                    db.Paciente.Add(new Paciente
                    {
                        PersonaId = persona.Id,
                        FRegistro = DateTime.Now,
                        UserName = "defaultUser"
                    });

                    db.SaveChanges();
                }
            }
            catch (Exception)
            {
                throw new WebFaultException(HttpStatusCode.BadRequest);
            }
        }
    }
}
