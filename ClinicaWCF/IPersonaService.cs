﻿using ClinicaWCF.Models;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace ClinicaWCF
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IPersonaService" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IPersonaService
    {
        [OperationContract]
        [WebGet(UriTemplate = "Personas", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<Persona> ConsultarPersonas();

        [OperationContract]
        [WebGet(UriTemplate = "Personas/{Id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Persona ConsultarPersona(string Id);

        [OperationContract]
        [WebInvoke(UriTemplate = "Personas", Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void RegistrarPersona(Persona paciente);

        [OperationContract]
        [WebInvoke(UriTemplate = "Personas", Method = "PUT", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void ActualizarPersona(Persona paciente);

        [OperationContract]
        [WebInvoke(UriTemplate = "Personas/{Id}", Method = "DELETE", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        void EliminarPersona(string Id);
    }
}
